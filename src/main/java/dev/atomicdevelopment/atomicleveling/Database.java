package dev.atomicdevelopment.atomicleveling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Database {

    private Connection connection;
    private static final Logger LOGGER = LoggerFactory.getLogger(Database.class);

    public void closeConnection() {
        try {
            if (!connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException error) {
            error.printStackTrace();
            LOGGER.error("Failed to close db connection");
        }
    }

    public Database() {
        File dataDir;
        if (!(dataDir = new File("data")).exists()) {
            boolean result = dataDir.mkdir();
            if (!result) {
                LOGGER.error("Failed to make data directory, exiting...");
                System.exit(1);
            }
        }
        try {
            connection = DriverManager.getConnection("jdbc:h2:./data;DB_CLOSE_ON_EXIT=FALSE", "admin", "");
            createTables();
        } catch (SQLException error) {
            error.printStackTrace();
        }
}

    private void createTables() {
        try {
            Statement statement = connection.createStatement();
            statement.addBatch("CREATE TABLE IF NOT EXISTS BotStats(Stat VARCHAR(255), Value INT)");

            statement.executeBatch();
            connection.commit();
            statement.close();
        } catch (SQLException error) {
            error.printStackTrace();
        }
    }

    public Optional<Boolean> doesBotStatsExist() {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT EXISTS(SELECT Stat FROM BotStats)");
            statement.executeQuery();
            ResultSet resultSet = statement.getResultSet();
            resultSet.next();
            boolean result = resultSet.getBoolean(1);
            resultSet.close();
            statement.close();
            return Optional.of(result);
        } catch (SQLException error) {
            error.printStackTrace();
            return Optional.empty();
        }
    }

    public boolean insertBotStats(Stat[] stats) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO BotStats (Stat, Value) VALUES (?, ?)");
            for (Stat stat : stats) {
                statement.setString(1, stat.toString());
                statement.setInt(2, stat.defaultValue);
                statement.executeUpdate();
            }
            statement.close();
            return true;
        } catch (SQLException error) {
            error.printStackTrace();
            return false;
        }
    }

    public Optional<HashMap<Stat, Integer>> getBotStats() {
        try {
            HashMap<Stat, Integer> returnHashMap = new HashMap<>();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM BotStats");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Stat stat = Stat.valueOf(resultSet.getString(1));
                Integer value = resultSet.getInt(2);
                returnHashMap.put(stat, value);
            }
            statement.close();
            resultSet.close();
            return Optional.of(returnHashMap);
        } catch (SQLException error) {
            error.printStackTrace();
            return Optional.empty();
        }
    }

    public boolean updateBotStats(HashMap<Stat, Integer> stats) {
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE BotStats SET Value=? WHERE Stat=?");
            for (Map.Entry<Stat, Integer> entry : stats.entrySet()) {
                statement.setInt(1, entry.getValue());
                statement.setString(2, entry.getKey().toString());
                statement.executeUpdate();
            }
            connection.commit();
            statement.close();
            return true;
        } catch (SQLException error) {
            error.printStackTrace();
            return false;
        }
    }
}
