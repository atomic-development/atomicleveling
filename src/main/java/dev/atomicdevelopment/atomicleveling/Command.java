package dev.atomicdevelopment.atomicleveling;

import net.dv8tion.jda.api.entities.Message;

public interface Command {
    boolean handleCommand(Message message);  // returns true if command was successful otherwise returns false
}
