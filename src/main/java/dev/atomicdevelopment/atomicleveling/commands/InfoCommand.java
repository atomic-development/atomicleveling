package dev.atomicdevelopment.atomicleveling.commands;

import dev.atomicdevelopment.atomicleveling.Command;
import dev.atomicdevelopment.atomicleveling.Stat;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;

import java.awt.*;

import static dev.atomicdevelopment.atomicleveling.Listeners.startTime;
import static dev.atomicdevelopment.atomicleveling.Listeners.stats;

public class InfoCommand implements Command {

    private static String formatTime(long seconds) {
        if (seconds < 60) {
            return String.format("%d Seconds", seconds);
        }
        long minutes = seconds / 60;

        if (minutes < 60) {
            return String.format("%d Minutes and %d Seconds", minutes, seconds - (minutes * 60));
        }
        long hours = minutes / 60;
        return String.format("%d Hours, %d Minutes and %d Seconds", hours, minutes - (hours * 60), seconds - (minutes * 60));
    }

    @Override
    public boolean handleCommand(Message message) {
        EmbedBuilder embed = new EmbedBuilder();
        User author = message.getAuthor();
        JDA jda = message.getJDA();
        // Get statistics
        int commandCount = stats.get(Stat.COMMAND_COUNT);
        int successfulCount = stats.get(Stat.SUCCESSFUL_COMMAND_COUNT);
        int failedCount = stats.get(Stat.UNSUCCESSFUL_COMMAND_COUNT);
        int guildCount = jda.getGuilds().size();
        int channelCount = jda.getTextChannels().size();
        int memberCount = jda.getUsers().size();

        String uptime = formatTime((System.currentTimeMillis() / 1000) - startTime);
        embed.setTitle("Bot info:")
                .setColor(new Color(49, 186, 156))
                .addField("Bot name", "```AtomicLeveling```", true)
                .addField("Bot Developer", "```Atomic#2062```", true)
                .addField("Version", "```1.0```", true)
                .addField("Uptime", String.format("```%s```", uptime), false)
                .addField("Commands executed", String.format("```%d```", commandCount), true)
                .addField("Commands successful", String.format("```%d```", successfulCount), true)
                .addField("Commands failed", String.format("```%d```", failedCount), true)
                .addField("Guilds", String.format("```%d```", guildCount), true)
                .addField("Text Channels", String.format("```%d```", channelCount), true)
                .addField("Members", String.format("```%d```", memberCount), true)
                .setAuthor(author.getAsTag(), null, author.getAvatarUrl());
        message.getChannel().sendMessageEmbeds(embed.build()).queue();
        return true;
    }
}
