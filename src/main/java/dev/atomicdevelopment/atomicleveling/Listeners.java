package dev.atomicdevelopment.atomicleveling;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import dev.atomicdevelopment.atomicleveling.commands.*;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.awt.*;
import java.util.HashMap;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

import static dev.atomicdevelopment.atomicleveling.Bot.db;
import static dev.atomicdevelopment.atomicleveling.Bot.toml;

public class Listeners extends ListenerAdapter {

    private static final HashMap<String, Command> commands = new HashMap<>();
    private static final String PREFIX = "a?";
    private static final Logger LOGGER = LoggerFactory.getLogger(Listeners.class);

    // bot statistics
    public static long startTime;
    public static HashMap<Stat, Integer> stats;

    @Nullable
    private Command getCommand(String commandName) {
        return commands.getOrDefault(commandName, null);
    }

    private void incrementStat(Stat stat) {
        stats.replace(stat, stats.get(stat) + 1);
    }

    @Override
    public void onReady(@NotNull ReadyEvent event) {

        // Load bot statistics from database
        Optional<Boolean> result = db.doesBotStatsExist();
        if (result.isPresent() && result.get()) {
            Optional<HashMap<Stat, Integer>> optionalStats = db.getBotStats();
            if (optionalStats.isEmpty()) {
                LOGGER.error("Failed to load statistics from the database");
                System.exit(1);
            }
            stats = optionalStats.get();
            LOGGER.info("Successfully loaded statistics from the database");
        } else {
            if (result.isEmpty()) {
                LOGGER.error("Failed to check database for statistics");
                System.exit(1);
            }
            boolean successful = db.insertBotStats(Stat.values());
            if (!successful) {
                LOGGER.error("Failed to add new statistics to the database");
                System.exit(1);
            }
            LOGGER.info("Successfully inserted new statistics to the database");
        }

        // add commands to hashmap
        addCommand("info", new InfoCommand());

        startTime = System.currentTimeMillis() / 1000;  // set the start time of the discord bot (for uptime)
        LOGGER.info(String.format("Successfully logged in as '%s'", event.getJDA().getSelfUser().getAsTag()));

        // Database update task
        Timer dbTimer = new Timer("DBUpdate-Thread");
        long interval = toml.getLong("dbUpdateInterval") * 1000;  // get the interval from config and convert to ms
        TimerTask dbTask = new TimerTask() {
            @Override
            public void run() {
                LOGGER.info("[Begin] Updating bot stats in database");
                db.updateBotStats(stats);
                LOGGER.info("[End] Finished updating bot stats in database");
            }
        };
        dbTimer.schedule(dbTask, interval, interval);  // add an initial delay as database will always be up to date at start
    }

    @Override
    public void onGuildMessageReceived(@NotNull GuildMessageReceivedEvent event) {

        // Command handing
        User user = event.getAuthor();
        if (event.isWebhookMessage() || user.isBot()) return;

        String message = event.getMessage().getContentRaw();
        if (message.length() == 0) return;  // join messages have a length of 0

        int prefixLength = PREFIX.length();
        if (!(message.substring(0, prefixLength).equals(PREFIX))) return;  // checks if message begins with the prefix

        int commandEnd = message.indexOf(" ");
        Command command;
        if (commandEnd == -1) {
            command = getCommand(message.substring(prefixLength));
        } else {
            command = getCommand(message.substring(prefixLength, commandEnd));
        }

        incrementStat(Stat.COMMAND_COUNT);

        if (command == null) {
            EmbedBuilder embed = new EmbedBuilder()
                    .setTitle("Invalid command")
                    .setDescription("The command you have entered could not be found")
                    .setColor(Color.RED);
            event.getChannel().sendMessageEmbeds(embed.build()).queue();
            incrementStat(Stat.UNSUCCESSFUL_COMMAND_COUNT);
        } else {
            if (command.handleCommand(event.getMessage())) {
                incrementStat(Stat.SUCCESSFUL_COMMAND_COUNT);
            } else {
                incrementStat(Stat.UNSUCCESSFUL_COMMAND_COUNT);
            }
        }
    }

    private void addCommand(String commandName, Command command) {
        commands.put(commandName, command);
    }
}
