package dev.atomicdevelopment.atomicleveling;

import com.moandjiezana.toml.Toml;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.cache.CacheFlag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.login.LoginException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static dev.atomicdevelopment.atomicleveling.Listeners.stats;

public class Bot {

    private static final Logger LOGGER = LoggerFactory.getLogger(Bot.class);
    public static Toml toml;
    private static JDA jda;
    public static Database db = new Database();

    private static void generateConfig() {
        try {
            InputStream defaultConfig = Bot.class.getResourceAsStream("/config.toml");
            assert defaultConfig != null;
            Files.copy(defaultConfig, Path.of("config.toml"));
        } catch (IOException error) {
            error.printStackTrace();
        }
        LOGGER.error("Config could not be found, a new one has been generated for you");
        System.exit(1);
    }

    public static void main(String[] args) {

        // Load toml config
        try {
            toml = new Toml().read(new FileInputStream("config.toml"));
        } catch (IOException error) {
            generateConfig();
        }

        // Get discord token
        String token = toml.getString("token");
        if (token == null || token.length() == 0) {
            LOGGER.error("No token has been defined within the config file");
            System.exit(1);
        }

        // Create JDA instance
        try {
            jda = JDABuilder.create(token, GatewayIntent.GUILD_MESSAGES, GatewayIntent.GUILD_MEMBERS)
                    .disableCache(CacheFlag.ACTIVITY, CacheFlag.VOICE_STATE, CacheFlag.CLIENT_STATUS, CacheFlag.EMOTE, CacheFlag.ONLINE_STATUS)
                    .addEventListeners(new Listeners())
                    .build();
        } catch (LoginException error) {
            LOGGER.error("Invalid discord token has been passed, exiting...");
            System.exit(1);
        }

        // shutdown hook
        Thread shutdownThread = new Thread(() -> {
            LOGGER.info("Shutting down...");
            jda.shutdown();
            boolean success = db.updateBotStats(stats);
            if (success) {
                LOGGER.info("Successfully updated bot statistics in database");
            } else {
                LOGGER.error("Could not update bot statistics, WARNING: stats will be lost");
            }
            db.closeConnection();
            LOGGER.info("Successfully exited");
        });
        shutdownThread.setName("Shutdown-Thread");
        Runtime.getRuntime().addShutdownHook(shutdownThread);
    }
}
