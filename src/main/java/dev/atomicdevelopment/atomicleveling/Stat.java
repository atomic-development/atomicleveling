package dev.atomicdevelopment.atomicleveling;

public enum Stat {
    COMMAND_COUNT(0),
    SUCCESSFUL_COMMAND_COUNT(0),
    UNSUCCESSFUL_COMMAND_COUNT(0);

    int defaultValue;

    Stat(int defaultValue) {
        this.defaultValue = defaultValue;
    }

    public int getDefaultValue() {
        return defaultValue;
    }
}
